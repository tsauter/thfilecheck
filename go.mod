require (
	github.com/VirusTotal/vt-go v0.0.0-20191029154732-b6fd15b76f39
	github.com/go-resty/resty/v2 v2.1.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.1
)

module gitlab.com/tsauter/thfilecheck

go 1.13
