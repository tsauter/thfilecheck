package libs

//
// WildFire API Reference
// https://docs.paloaltonetworks.com/content/dam/techdocs/en_US/pdf/wildfire/9-0/wildfire-api/wildfire-api.pdf
//

import (
	"fmt"

	"github.com/go-resty/resty/v2"
)

type Wildfire struct {
	apikey string
	Debug  bool
}

type WildfireResponse struct {
	//XMLName xml.Name    `xml:"wildfire"`
	Verdict VerdictInfo `xml:"get-verdict-info"`
}

type VerdictInfo struct {
	//XMLName xml.Name `xml:"get-verdict-info"`
	SHA256 string `xml:"sha256"`
	MD5    string `xml:"md5"`
	Status int    `xml:"verdict"`
	Text   string `xml:"-"`
}

func NewWildfire(apikey string) *Wildfire {
	return &Wildfire{apikey: apikey}
}

func (w *Wildfire) GetVerdict(hash string) (*VerdictInfo, error) {
	client := resty.New()

	var wfresponse WildfireResponse

	resp, err := client.R().
		SetFormData(map[string]string{
			"apikey": w.apikey,
			"hash":   hash,
		}).
		SetResult(&wfresponse).
		Post("https://wildfire.paloaltonetworks.com/publicapi/get/verdict")
	if err != nil {
		return nil, fmt.Errorf("Failed to get verdict: %s", err)
	}

	if resp.IsError() {
		return nil, fmt.Errorf("REST error: %s", resp.Status())
	}

	switch wfresponse.Verdict.Status {
	case 0:
		wfresponse.Verdict.Text = "benign"
	case 1:
		wfresponse.Verdict.Text = "malware"
	case 2:
		wfresponse.Verdict.Text = "grayware"
	case -100:
		wfresponse.Verdict.Text = "pending, the sample exists, but there is currently no verdict"
	case -101:
		wfresponse.Verdict.Text = "error"
	case -102:
		wfresponse.Verdict.Text = "unknown, cannot find sample record in the database"
	case -103:
		wfresponse.Verdict.Text = "invalid hash value"
	default:
		wfresponse.Verdict.Text = "error/invalid status"
	}

	if w.Debug {
		fmt.Printf("%#v\n", wfresponse.Verdict)
	}
	return &wfresponse.Verdict, nil
}
