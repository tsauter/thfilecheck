package libs

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"strings"
)

func GetMD5(filename string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", fmt.Errorf("Failed to open file: %v", err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", fmt.Errorf("Failed to read file: %v", err)
	}

	return strings.ToLower(fmt.Sprintf("%x", h.Sum(nil))), nil
}

func GetSHA1(filename string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", fmt.Errorf("Failed to open file: %v", err)
	}
	defer f.Close()

	h := sha1.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", fmt.Errorf("Failed to read file: %v", err)
	}

	return strings.ToLower(fmt.Sprintf("%x", h.Sum(nil))), nil
}

func GetSHA256(filename string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", fmt.Errorf("Failed to open file: %v", err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", fmt.Errorf("Failed to read file: %v", err)
	}

	return strings.ToLower(fmt.Sprintf("%x", h.Sum(nil))), nil
}
