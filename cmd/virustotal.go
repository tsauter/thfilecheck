/*
Copyright © 2019 Thorsten Sauter <t.sauter@viastore.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"sort"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	vt "github.com/VirusTotal/vt-go"
)

// virustotalCmd represents the virustotal command
var virustotalCmd = &cobra.Command{
	Use:   "virustotal",
	Short: "The VirusTotal commands are used to interact with the VirusTotal database.",
	Long: `The available subcommands can be used to query or execute tasks against the VirusTotal database.

A valid VirusTotal API key must be specified.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Please use one of the available subcommands to interact with the VirusTotal database.\n\n")
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(virustotalCmd)
}

func QueryVirusTotalByHash(cmd *cobra.Command, filehash string) {
	apikey := viper.GetString("vt-apikey")
	if apikey == "" {
		fmt.Printf("VirusTotal API key is emptry. Aborting.\n")
		return
	}

	debug := viper.GetBool("debug")

	if debug {
		fmt.Printf("SHA-256: %s\n", filehash)
	}

	client := vt.NewClient(apikey)

	file, err := client.GetObject(vt.URL("files/%s", filehash))
	if err != nil {
		fmt.Printf("Failed to query VirusTotal API: %v\n", err)
		return
	}

	results, err := file.Get("last_analysis_results")
	if err != nil {
		fmt.Printf("Failed to get last analysis results: %v\n", err)
		return
	}

	results_map := results.(map[string]interface{})

	var engines []string
	total_engines := 0

	for name, v := range results_map {
		total_engines++
		engine_results := v.(map[string]interface{})
		if engine_results["result"] != nil {
			engines = append(engines, name)
		}
	}

	fmt.Printf("%s: detected by %d engines (%d total)\n", file.ID(), len(engines), total_engines)

	sort.Strings(engines)
	for _, k := range engines {
		engine_results := results_map[k].(map[string]interface{})
		fmt.Printf(" - %-25s  =>  %s\n", k, engine_results["result"])
		if debug {
			fmt.Printf("   %#v\n", engine_results)
		}
	}

	return
}
