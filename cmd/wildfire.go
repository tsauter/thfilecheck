/*
Copyright © 2019 Thorsten Sauter <t.sauter@viastore.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/tsauter/thfilecheck/libs"
)

// wildfireCmd represents the wildfire command
var wildfireCmd = &cobra.Command{
	Use:   "wildfire",
	Short: "The wildfire commands are used to interact with the WildFire cloud.",
	Long: `The available subcommands can be used to query or execute tasks against the Palo Alto
WildFire cloud.

A valid Wildfire API key must be specified.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Please use one of the available subcommands to interact with the WildFire cloud.\n\n")
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(wildfireCmd)
}

func QueryWildFireByHash(cmd *cobra.Command, filehash string) {
	apikey := viper.GetString("wf-apikey")
	if apikey == "" {
		fmt.Printf("Palo Alto Wildfire API key is empty. Aborting.\n")
		return
	}

	debug := viper.GetBool("debug")

	if debug {
		fmt.Printf("SHA-256: %s\n", filehash)
	}

	wf := libs.NewWildfire(apikey)
	wf.Debug = debug

	verdict, err := wf.GetVerdict(filehash)
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}

	fmt.Printf("%s: %s\n", verdict.SHA256, verdict.Text)
	return
}
