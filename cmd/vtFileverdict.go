/*
Copyright © 2019 Thorsten Sauter <t.sauter@viastore.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/tsauter/thfilecheck/libs"
)

// vtFileverdictCmd represents the vtFileverdict command
var vtFileverdictCmd = &cobra.Command{
	Use:   "fileverdict",
	Short: "Get a VirusTotal verdict for the specified filename",
	Long: `Queries the VirusTotal database to get a verdict of the specified file checksum.
The checksum will be calculated as a SHA-256 hash from the specified file.

A valid VirusTotal API key must be specified.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Printf("At least one filename is necessary. Aborting.\n")
			return
		}

		for _, filename := range args {
			filehash, err := libs.GetSHA256(filename)
			if err != nil {
				fmt.Printf("Failed to calculate checksum: %v", err)
				return
			}

			QueryVirusTotalByHash(cmd, filehash)
			if len(args) < 1 {
				fmt.Printf("At least one hash value is necessary. Aborting.\n")
				return
			}

		}
	},
}

func init() {
	virustotalCmd.AddCommand(vtFileverdictCmd)
}
