/*
Copyright © 2019 Thorsten Sauter <t.sauter@viastore.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "thfilecheck",
	Short: "A Threat Hunting application to validate files.",
	Long: `A Threat Hunting application to validate files or file hashes against public
available databases.
The results can help to get a first estimation of wether the file is mallicious.

Most of the databases needs an active subscription.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.thfilecheck.yaml)")

	rootCmd.PersistentFlags().StringP("wf-apikey", "", "", "Palo Alto WildFire API key")
	viper.BindPFlag("wf-apikey", rootCmd.PersistentFlags().Lookup("wf-apikey"))

	rootCmd.PersistentFlags().StringP("vt-apikey", "", "", "VirusTotal API key")
	viper.BindPFlag("vt-apikey", rootCmd.PersistentFlags().Lookup("vt-apikey"))

	rootCmd.PersistentFlags().BoolP("debug", "", viper.GetBool("debug"), "Enable extended debugging")
	viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".thfilecheck" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath(".")
		viper.SetConfigName(".thfilecheck")
	}

	viper.SetEnvPrefix("THFILECHECK")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		//fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
