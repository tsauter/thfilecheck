/*
Copyright © 2019 Thorsten Sauter <t.sauter@viastore.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// filehashCmd represents the filehash command
var filehashCmd = &cobra.Command{
	Use:   "filehash",
	Short: "The filehash commands are used to calculate various file hashes.",
	Long: `The available subcommands can be used to calculate different file hashes of the specified
files.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Please use one of the available subcommands to interact with the filehash cloud.\n\n")
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(filehashCmd)
}
