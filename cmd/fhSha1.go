/*
Copyright © 2019 Thorsten Sauter <t.sauter@viastore.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/tsauter/thfilecheck/libs"
)

// fhSha1Cmd represents the fhSha1 command
var fhSha1Cmd = &cobra.Command{
	Use:   "sha1",
	Short: "Calculate the SHA-1 checksum",
	Long:  `Calculate the SHA-1 checksum of the specified file names.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Printf("At least one filename is necessary. Aborting.\n")
			return
		}

		for _, filename := range args {
			checksum, err := libs.GetSHA1(filename)
			if err != nil {
				fmt.Printf("Failed to calculate checksum: %v\n", err)
				return
			}

			fmt.Printf("%s: %s\n", filename, checksum)
		}

	},
}

func init() {
	filehashCmd.AddCommand(fhSha1Cmd)
}
